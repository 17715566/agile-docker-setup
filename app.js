const express = require("express");
const dotenv = require('dotenv');
dotenv.config({path:'./config.env'});
const mongoose = require('mongoose');

const app = express()


const DB = process.env.uri;
 mongoose.connect(DB).then((con) =>{
    console.log(con.connections)
    console.log('DB connected Successfully')
 }).catch(error=> console.log(error));

// Starting the server on port 4001

const port = 4001
app.listen(port, () => {
 console.log(`App running on port ${port} ..`)
})