const mongoose = require('mongoose')
const validator = require('validator')
const { setMaxListeners } = require('../app')

const userScheme = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!']
    },
    email: {
        type: String,
        required: [true, 'Please [rovide your email!'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: "default-tag"
    },
    role: {
        type: String,
        enum:['user', 'sme', 'pharmacist', 'admin'],
        default: 'user'
    },
    password: {
        type: String,
        required:[true, "Please provide a password"],
        minlength: 8,
        select: false
    },
    active: {
        type: Boolean,
        default: true,
        select: false
    },
})

const User = mongoose.model("User", userScheme)
module.exports = User